/* APP INSTELLINGEN */
const intervalSeconds = 10000; // Om de zoveel miniseconden van pagina veranderen			
const maxStep 		  = 6;	  // Houdt bij hoevaal stappen / pagina's er maximaal zijn ( Verhogen als er een nieuwe pagina erbij komt )
var notificationSound = new Audio("resources/audio/notification.mp3");	// Notificatie geluid

/* GLOBALE VARIABELEN */
var currentStep       = 1;	  // Houdt de huidige stap / pagina bij
var minStep           = 1;	  // Houdt bij wat de eerste stap/pagina is
var timedPageSwitch   = null; // Initial Interval var (leeg)

/* Navigatie-buttons - Vorige & Volgende */
const nextStep        = $('#nextStep');
const previousStep    = $('#previousStep');

/* Navigatie-buttons - Eerste & Laatste */
const fastForward 	  = $('#fastForward');
const fastBackward    = $('#fastBackward');

/* Kijkt naar huidige stap en bepaalt of knoppen disabled moeten worden of niet */
function checkCurrentStep(currentStep) {
	
	// Als eerste stap/pagina actief is, deactiveer buttons "vorige" en "eerste"
	if (currentStep === minStep) { 
		previousStep.css("opacity","0.5").css("z-index","-10");
		fastBackward.css("opacity","0.5").css("z-index","-10");
	}else{
		previousStep.css("opacity","1").css("z-index","10");
		fastBackward.css("opacity","1").css("z-index","10");
	}

	// Als laatste stap/pagina actief is, deactiveer buttons "volgende" en "laatste"
	if (currentStep === maxStep) { 
		nextStep.css("opacity","0.5").css("z-index","-10");
		fastForward.css("opacity","0.5").css("z-index","-10");
	}else{
		nextStep.css("opacity","1").css("z-index","10");
		fastForward.css("opacity","1").css("z-index","10");
	}
}


/* Generieke functie om request te maken om vorige/volgende pagina's op te halen a.d.h.v. "currentStep" */
function requestStep(currentStep) {

	/* Haal de pagina op */
	$.get("step" + currentStep + ".html", function(data){})

	/* Als AJAX-Request gelukt is */
	.done(function(data){
	    checkCurrentStep(currentStep);
		$('.container').html(data).hide().fadeIn(1000);
	})

	/* Als AJAX-Request niet gelukt is */
	.fail(function(){
		$('.container').html("Failed");
	})
}

/* Initial page-load */
$(function(){
	checkCurrentStep(1); // Kijkt wat de huidige stap is (Om te bepalen wat de vorige/volgende wordt)	
	requestStep(1);    // Haalt de pagina op
});

/* Functie: Page switch om de x seconden */
function switchPageInterval( condition ){

	// (Re)start de interval
	if( condition === true ){

		 timedPageSwitch = setInterval( function() {
			// Als laatste stap bereikt is, loop weer vanaf het begin ( Eerste pagina )
			if (currentStep === minStep){
				previousStep.css("visibility","1").css("z-index","10");
				fastBackward.css("opacity","1").css("z-index","10");
			}
			if (currentStep === maxStep){
				previousStep.css("opacity","0.5").css("z-index","-10");
				fastBackward.css("opacity","0.5").css("z-index","-10");
			}
			// Switch page: Ga naar volgende pagina
			nextView();

		  }, intervalSeconds);
	}

	// Stop de interval
	else if( condition === false ){
		timedPageSwitch = clearInterval(timedPageSwitch);

		if(!timedPageSwitch){
			$(".resetInterval").css("visibility","hidden").css("z-index","-10");
			
		}else{
			$(".resetInterval").css("visibility","visible").css("z-index","10");
		}
		console.log("The timed interval has been stopped");
	}
	
	// Geen argumenten meegegeven tijdens initialiseren van de functie
	else{
		console.log( "No arguments given for function: switchPageInterval();" );
	}
}

/* Voer de functie uit als de pagina geladen is */
switchPageInterval(true ,3000);

/* Stop de page-switch interval als er geklikt wordt op de Navigatie-buttons */
$(".goToNext, .goToPrevious, .goToFirst, .goToLast").on("click", function(){
	switchPageInterval(false);
	$(".resetInterval").css("visibility","visible");
})


/* Volgende stap functie */
function nextView() {

	if(currentStep === 3){
		
	}

	if (currentStep != maxStep) { // Als de app niet op de laatste pagina zit
		currentStep += 1;		  // Ga naar volgende stap/pagina
		requestStep(currentStep);
		console.log("Huidige pagina: " + currentStep);
	}else{
		currentStep = 1;		 
		requestStep(currentStep);
		console.log("Huidige pagina: " + currentStep);
	}

	/* Schakel bepaalde navigatie-buttons uit a.d.h.v. huidige pagina */
	if(currentStep == maxStep){ // Verberg Next en Last buttons als laatste pagina actief is
		nextStep.css("opacity","0.5").css("z-index","-10");
		fastForward.css("opacity","0.5").css("z-index","-10");
	}
	if(currentStep == minStep){ // Verberg Previous en First buttons als laatste pagina actief is
		previousStep.css("opacity","0.5").css("z-index","-10");
		fastBackward.css("opacity","0.5").css("z-index","-10");
	}
	
	/* Voer een testfunctie uit als Pagina/Stap 3 geladen is */
	if(currentStep === 3){
		//console.log("GELUKT: Test op pagina 3 \n Voer javascript uit op specifieke pagina's of stappen");
	}
}

/* Vorige stap functie */
function previousView() {
	
	if (currentStep != minStep) { // Als de app niet op eerste pagina zit
		currentStep -= 1;		  // Ga naar vorige stap/pagina
		requestStep(currentStep);
		console.log("Huidige pagina: " + currentStep);
	}
}

/* Fast forward- and backward button functies ( Eerste & Laatste ) */
fastForward.on("click", function () {
	checkCurrentStep(); 
	currentStep = maxStep;
	requestStep(currentStep);
})
fastBackward.on("click", function () {
	checkCurrentStep();
	currentStep = minStep;
	requestStep(currentStep);
})

/* Restart de interval on-click */
$(".resetInterval").on("click", function () {

	if(!timedPageSwitch){
		$(".resetInterval").css("visibility","hidden");
		switchPageInterval(true, 3000);
		console.log("Restarted the interval!");

	}else{
		$(".resetInterval").css("visibility","visible");
	}

})

/* Hover-Effects voor de navigatiebuttons */
$(".goToNext, .goToPrevious, .goToFirst, .goToLast, .resetInterval").hover(function(){
	$(this).addClass("btnHover");
}).mouseout(function(){
	$(this).removeClass("btnHover");
})

/* Notificatie functie */
$("#weatherInfo").on("click", function(){

	var notificationContent = "<b>Er is een nieuwe Credit Safe aanmelding binnen!<b>"; // Notificatie tekst
	notificationSound.play(); // Speel geluid af

	// Slide-down, geef notificatie content mee, wacht x seconden en slide weer naar boven
	$("#notifications").slideDown(1000).html(notificationContent).delay(10000).slideUp(1000);

});


$(".goToNext, .goToPrevious, .goToFirst, .goToLast, .resetInterval").mousedown( function(){
	$(this).css("transform", "scale(0.9, 0.9)"); 
}).mouseout( function(){
	$(this).css("transform", "scale(1, 1)"); 
})