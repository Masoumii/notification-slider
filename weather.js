$(function(){

        

    /* APP SETTINGS */
    const apiKey      = "ac953f185f3e30fdf061e93ab48e2bc9";  // OpenWeatherMap API Key
    const units       = "metric";                            // metric(Celcius) of imperial(Fahrenheit)
    const cityToQuery = "Laren, Nederland";                  // Chosen city for the weather info in format:"Laren" of "Laren, Nederland"
    const refreshRate = 120000;                              // ( Iedere 2 minuten )

                    /* Main function */
                    function getWeather(){
                   
                    /* Get weather data based on user-input from the API, extract the data and assign to variables */
                    $.get("https://api.openweathermap.org/data/2.5/weather?q="+cityToQuery+"&units="+units+"&appid="+apiKey, function(data){  

                            /* Weather Object */
                            let weatherMain        = data.weather[0].main;
                            let weatherDescription = data.weather[0].description;
                            let weatherIcon        = data.weather[0].icon;

                            /* Main Object */
                            let mainTemp           = Math.round(data.main.temp);
                            let mainPressure       = data.main.pressure;
                            let mainHumidity       = data.main.humidity;
                            let mainTempMin        = data.main.temp_min;
                            let mainTempMax        = data.main.temp_max;

                            /* Visibility Object */
                            let Visibility         = data.visibility;

                            /* Wind Object */
                            let windSpeed          = data.wind.speed;
                            let windDegree         = data.wind.deg;

                            /* Cloud Object */
                            let Cloud              = data.clouds.all;

                            /* System Object */
                            let systemSunriseHour = new Date(data.sys.sunrise*1000).getHours();
                            let systemSunriseMin  = new Date(data.sys.sunrise*1000).getMinutes();
                            let systemSunsetHour  = new Date(data.sys.sunset*1000).getHours();
                            let systemSunsetMin   = new Date(data.sys.sunset*1000).getMinutes();

                            /* Function to format sunrise minutes */
                            function addLeadingZeroSunrise(minute){
                                    if(minute<10){
                                            systemSunriseMin = "0"+minute;
                                     }else{
                                            systemSunriseMin = minute;
                                     }
                                    return systemSunriseMin;
                            }

                            /* Function to format sunset minutes */
                            function addLeadingZeroSunset(minute){
                                    if(minute<10){
                                            systemSunsetMin = "0"+minute;
                                    }else{
                                           systemSunsetMin = minute;
                                    }
                                    return systemSunsetMin;
                            }

                            /* Format sunrise and sunset minutes and return values */
                            addLeadingZeroSunrise(systemSunriseMin);
                            addLeadingZeroSunset(systemSunsetMin);

                            /* Combine hours and minutes and assign to variable */
                            let systemSunrise     = systemSunriseHour+":"+systemSunriseMin;
                            let systemSunset      = systemSunsetHour+":"+systemSunsetMin;

                            /* Background Video-file Sources */
                            let videoRaining        = "raining.mp4";
                            let videoSunny          = "sunny.mp4";
                            let videoStorm          = "storm.mp4";
                            let videoSnow           = "snowing.mp4";
                            let videoFoggy          = "foggy.mp4";
                            let videoCloudy         = "cloudy.mp4";
                            let videoLightCloudy    = "light-cloudy.mp4"
                            let videoSand           = "sandstorm.mp4";

                            /* Determine Background & Video source variables depending on the current weather Conditions */
                            switch (weatherMain) {

                                    case "Clear":
                                            backgroundVideoSrc = videoSunny;
                                    break;

                                    case "Rain":
                                            backgroundVideoSrc = videoRaining;
                                    break;

                                    case "Drizzle":
                                            backgroundVideoSrc = videoRaining;
                                    break;

                                    case "Storm":
                                            backgroundVideoSrc = videoStorm;
                                    break;

                                    case "Snow":
                                            backgroundVideoSrc = videoSnow;
                                    break;

                                    case "Smoke":
                                            backgroundVideoSrc = videoFoggy;
                                    break;

                                    case "Haze":
                                            backgroundVideoSrc = videoFoggy;
                                    break;

                                    case "Mist":
                                            backgroundVideoSrc = videoFoggy;
                                    break;

                                    case "Fog":
                                            backgroundVideoSrc = videoFoggy;
                                    break;

                                    case "Clouds":
                                            backgroundVideoSrc = videoLightCloudy;
                                    break;

                                    case "Sand":
                                            backgroundVideoSrc = videoSand;
                                    break;

                                    default:
                                    alert("Huidige beschrijving van het weer (F-Cloudy) is nog niet gedefineerd! \n Raadpleeg de ontwikkelaar om deze beschrijving erin te zetten.");
                            }

                                    /* Initialise the Weather App */

                                    // Video
                                    videoToPlay = $("#backgroundVideoSrc").attr("src", "resources/videos/"+backgroundVideoSrc);
                                    let backgroundVideo = $("#backgroundMovie");
                                    backgroundVideo.append(videoToPlay);
                                    backgroundVideo[0].load();
                                    backgroundVideo[0].play();

                                    // Information
                                    $("#weatherInfo").html(
                                            "<img src='http://openweathermap.org/img/w/"+weatherIcon+".png' width='85'><br>"+cityToQuery+"<br><br>"+
                                            "<img src='resources/images/temp.svg' width='35' title='Temperature'>&nbsp;"+mainTemp+"℃"+"<br><br>"+
                                            "<img src='resources/images/humidity.svg' width='35' title='Humidity'>&nbsp;"+mainHumidity+"%"+"<br><br>"+
                                            "<img src='resources/images/sunrise.svg' width='35' title='Sunrise'>&nbsp;"+systemSunrise+"<br><br>"+
                                            "<img src='resources/images/sunset.svg' width='35' title='Sunset'>&nbsp;"+systemSunset+"<br><br>"
                                    ).fadeIn();
                                    
                    /* If (response)status 404 or City not found */                 
                    }).fail(function(){ 
                            $("#weatherInfo").hide().html("Stad niet gevonden of geen reactie van API.<br><br>Gebruik a.u.b. Engelse stadsnamen<br> of probeer het later nog eens!").fadeIn();
                    });

                }

                /* Run function on start-up */
                getWeather();

                 /* Update weather info every minute (refreshRate) */
                 setInterval( getWeather, refreshRate);

               
});